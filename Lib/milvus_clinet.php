<?php


namespace kaycn\milvusphp;
//v1.0.1
use GuzzleHttp\Client;
class milvus_clinet
{
    protected $host;

    protected $port;

    public function __construct($host,$port=19121)
    {
            $this->host = $host;
            $this->port = $port;
    }
    /**
    * @author kaycn
     * @purpose 连接milvus
     */
    protected function connect()
    {
        return $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->host.':'.$this->port,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
    }
    //连接状态
    public function state()
    {
        $res = $this->connect()
           ->request("GET","/state",["header"=>["accept"=>"application/json"]])
           ->getBody()->getContents();
        return json_decode($res,true);
    }

    public function collections()
    {
        $res = $this->connect()
            ->request("GET","/collections",["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }

    public function create_collection($collection_name,$dimension,$index_file_size,$metric_type)
    {
        $data = ["collection_name"=>$collection_name,"dimension"=>$dimension,"index_file_size"=>$index_file_size,"metric_type"=>$metric_type];
        $res = $this->connect()
            ->request("POST","/collections",["header"=>["accept"=>"application/json"],"json"=>$data])
            ->getBody();
        return json_decode($res,true);
    }

    //获取集合详情
    public function get_collection($collection_name)
    {
        $res = $this->connect()
            ->request("GET","/collections/".$collection_name,["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }

    //

    public function get_collection_stat($collection_name)
    {
        $res = $this->connect()
            ->request("GET","/collections/".$collection_name.'?info=stat',["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }
    //删除集合
    public function del_collections($collection_name)
    {
        $res = $this->connect()
            ->request("DELETE","/collections/".$collection_name,["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }

    //查看索引
    public function get_indexes($collection_name)
    {
        $res = $this->connect()
            ->request("GET","/collections/".$collection_name.'/indexes',["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }

    //更新索引
    public function update_indexes($collection_name,$index_type,$params)
    {
        $data = ["index_type"=>$index_type,"params"=>$params];
        $res = $this->connect()
            ->request("POST","/collections/".$collection_name.'/indexes',["header"=>["accept"=>"application/json"],"json"=>$data])
            ->getBody();
        return json_decode($res,true);
    }

    //删除索引
    public function del_indexes($collection_name)
    {
        $res = $this->connect()
            ->request("DELETE","/collections/".$collection_name.'/indexes',["header"=>["accept"=>"application/json"]])
            ->getBody();
        return json_decode($res,true);
    }

    /**
     * @分区操作
     * @param $collection_name string
     * @param $partition string
     * @param $method string GET,POST,DELETE
     * @return array
     */
    public function partition($collection_name,$partition,$method)
    {
        $data = ["partition_tag"=>$partition];
        $res = $this->connect()
            ->request($method,"/collections/".$collection_name.'/partitions',["header"=>["accept"=>"application/json"],"json"=>$data])
            ->getBody();
        return json_decode($res,true);
    }

    /**
     * @向量插入操作
     * @param $collection_name string
     * @param $vectors array
     * @param null $ids
     * @param null $partition
     * @return mixed array
     */
    public function vectors($collection_name,$vectors,$ids=null,$partition=null)
    {
        if($partition == null)
        {
            $data = $ids==null?["vectors"=>$vectors]:["vectors"=>$vectors,"ids"=>$ids];
        }else{
            $data = $ids==null?["partition_tag"=>$partition,"vectors"=>$vectors]:["partition_tag"=>$partition,"vectors"=>$vectors,"ids"=>$ids];
        }
        $res = $this->connect()
            ->request("POST","/collections/".$collection_name.'/vectors',["header"=>["accept"=>"application/json"],"json"=>$data])
            ->getBody();
        return json_decode($res,true);
    }

    /**
     * @查询向量
     * @param $collection_name
     * @param $vectors
     * @param int $topk
     * @param null $params
     * @param null $partition
     * @return mixed
     */
    public function search_vectors($collection_name,$vectors,$topk=10,$params=null,$partition=null)
    {

        $data = $partition == null? ["vectors"=>$vectors,"topk"=>$topk,"params"=>$params]:["partition_tags"=>$partition,"vectors"=>$vectors,"topk"=>$topk,"params"=>$params];
        $res = $this->connect()
            ->request("PUT","/collections/".$collection_name.'/vectors',["header"=>["accept"=>"application/json"],"json"=>["search"=>$data]])
            ->getBody();
        return json_decode($res,true);
    }

    /**
     * @
     * @param $collection_name
     * @param $id array
     * @return mixed
     */
    public function del_vectors($collection_name,$id)
    {
        $res = $this->connect()
            ->request("DELETE","/collections/".$collection_name.'/vectors',["header"=>["accept"=>"application/json"],"json"=>["delete"=>$id]])
            ->getBody();
        return json_decode($res,true);
    }

}