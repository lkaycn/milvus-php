# milvus-php

#### 介绍

milvus php 客户端，基于Milvus RESTful API封装\
https://github.com/milvus-io/milvus/tree/master/core/src/server/web_impl

#### 安装教程
composer require kaycn/milvusphp

#### 使用说明
见client.php

